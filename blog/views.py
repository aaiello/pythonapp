from django.shortcuts import render
from blog.serializers import *
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import *
from rest_framework import status

@api_view(['POST'])
def add_review(request):
	context  = {
		"request": request
	}
	review_serializer = ReviewSerializer(data=request.data, context=context)
	if review_serializer.is_valid():
		review_serializer.save()
		return Response({"data": "Review added successfully"}, status=status.HTTP_201_CREATED)
	else:
		error_details = []
		for key in review_serializer.errors.keys():
			error_details.append({"field": key, "message": review_serializer.errors[key][0]})

		data = {
				"Error": {
					"status": 400,
					"message": "Your submitted data was not valid - please correct the below errors",
					"error_details": error_details
					}
                }

		return Response(data, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_all_reviews(request):
	review_obj = Review.objects.filter(author = request.user)
	ret = []
	for review in review_obj:
		ret.append ( ReviewSerializer(review).data )
	
	return Response({"data": ret}, status=status.HTTP_200_OK)	