from django.db import models
from django.utils import timezone

class Post(models.Model):
	author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
	title = models.CharField(max_length=200)
	text = models.TextField()
	created_date = models.DateTimeField(default=timezone.now)
	published_date = models.DateTimeField(blank=True, null=True)

	def publish(self):
		self.published_date = timezone.now()
		self.save()

	def __str__(self):
		return self.title

class Review(models.Model):
	author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
	created_date = models.DateTimeField(default=timezone.now)
	post_id = models.IntegerField(default=1)
	rating = models.IntegerField(default=1)
	title = models.CharField(max_length=64, default="title")
	summary = models.CharField(max_length=10000, default="summary")
	ip_address = models.CharField(max_length=16, default="0.0.0.0")
	company = models.CharField(max_length=64, default="company")