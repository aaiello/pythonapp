from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault
from .models import *

class ReviewSerializer(serializers.Serializer):
	rating = serializers.IntegerField()
	post_id = serializers.IntegerField()
	title = serializers.CharField(max_length=250)
	summary = serializers.CharField(max_length=250)
	company = serializers.CharField(max_length=250)
	
	def create(self, validated_data):
		review_obj = Review(**validated_data)
		review_obj.author = self.context.get("request").user

		x_forwarded_for = self.context.get("request").META.get('HTTP_X_FORWARDED_FOR')
		if x_forwarded_for:
			ip = x_forwarded_for.split(',')[0]
		else:
			ip = self.context.get("request").META.get('REMOTE_ADDR')

		review_obj.ip_address = ip
		review_obj.save()
		return review_obj