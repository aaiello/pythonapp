# Generated by Django 2.0.5 on 2018-05-02 17:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20180502_1319'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review',
            old_name='text',
            new_name='company',
        ),
        migrations.AddField(
            model_name='review',
            name='ip_address',
            field=models.CharField(default='a', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='review',
            name='rating',
            field=models.CharField(default='a', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='review',
            name='summary',
            field=models.CharField(default='a', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='review',
            name='title',
            field=models.CharField(default='a', max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='review',
            name='author',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='review',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
