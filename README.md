# Project Name

Backend Practical

## Installation

Clone this project <br />
<br />
Create enviroment<br />
install: pip install django<br />
install: pip install djangorestframework<br />

<br />
Create superuser: <br />
python manage.py createsuperuser <br />
Username: admin <br />
Email address: admin@admin.com <br />
Password: aaiello1234 <br />
Password (again): aaiello1234 <br />
<br />
Create database<br />
python manage.py makemigrations<br />
python manage.py migrate<br />
<br />
Run on localhost<br />
python manage.py runserver<br />
<br />
<br />
## Unit test

For unit test download https://www.getpostman.com/<br />
Import BPWS.postman_collection.json file from git<br />
<br />
First try Add Review and review get all. Should get access error<br />
Then try "Api review get token". Check that user name and password are the same you install.<br />
Then you should get access token. Use this token in the others calls headers. <br />
Headers should look like this:<br />
Content-Type:application/json<br />
Authorization:Token 03592688397c3c467c340073ed7b6eee92740dd3<br />
<br />
Now try again add and get web service. Should respond as expected<br />
Try "add revision with no valid parameters" to test wrong post<br />
<br />
Create another user and repeat steps to check that user can see only he's post<br />
<br />
Log to 127.0.0.1:8000/admin with superuser to see all post. Get doesn't show private information such as author and create time. Super user can see it in admin portal.<br />
<br />
I didn't complete revier metadata because suposed to be a django user, soo information should be in system, only user id is necesary in review. The same just for simplification don't validate post id as reference, just a number.<br />

## Usage



## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Write license